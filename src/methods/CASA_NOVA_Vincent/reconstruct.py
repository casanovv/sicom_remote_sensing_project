"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import reconstruction_functions as funct 


def run_reconstruction(pan, ms1, ms2):
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Performing the reconstruction.
    ms2_rescale = funct.rescale_ms2(ms2,pan.shape)
    reconstruct_img_hp = funct.high_pass_fusion_v2_ms_img(ms2_rescale,pan,alpha=0.7,N=8)
    reconstruct_img = funct.reconstruct_pca_img_ms(reconstruct_img_hp,pan,nb_components=5)
    return reconstruct_img 




# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
