import numpy as np
import sys
sys.path.append('..\..\..')
import src.utils as utils
from sklearn import decomposition
import skimage.transform as transform
import scipy.signal as signal

# EVALUATE RESULTS:
def evaluate_reconstruction(img,reconstruct_img,title):
    #To compute metrics and print them
    psnr = utils.psnr(img,reconstruct_img)
    if len(img.shape)<3: #The ssim metric need an array with 3 dimensions
        img = img.reshape(img.shape +(1,))
    if len(reconstruct_img.shape)<3:
        reconstruct_img = reconstruct_img.reshape(reconstruct_img.shape +(1,))
        
    ssim = utils.ssim(img,reconstruct_img)
    print(f'{title}= PSNR: {psnr:.2f} , SSIM: {ssim:.4f}')

# PREPROCESS DATAS:
def rescale_ms1(ms1,shape):
    return transform.rescale(ms1, 2, anti_aliasing=True, channel_axis=2)[:shape[0],:shape[1]]

def rescale_ms2(ms2,shape):
    return transform.rescale(ms2, 4, anti_aliasing=True, channel_axis=2)[:shape[0],:shape[1]]

# RECONSTRUCTION METHODS:

# A. High Pass Filter:
def local_mean_img(img,size_filter):
    #To compute the local mean of an image with a filter of size size_filter*size_filter
    filter = np.ones((size_filter,size_filter)) * (1/size_filter**2)
    mean_img = signal.convolve2d(img,filter,mode='same')
    return mean_img

## 1st method: Using img_new = img + alpha*(pan - pan_avg)
def high_pass_fusion(img_to_reconstruct,img_pan,alpha,N):
    #Here the aim is to fusion the panchromatic image with one dimension of the multispectral image
    #img_to_reconstruct_mean = local_mean_img(img_to_reconstruct,N)
    img_pan_HF = img_pan - local_mean_img(img_pan,N) #High frequencies of the panchromatic image
    img_reconstruct = img_to_reconstruct + alpha * img_pan_HF
    return img_reconstruct

def high_pass_fusion_ms_img(img_ms,img_pan,alpha,N):
    #Here the aim is to applied the high pass filter to each dimension of the multispectral image
    img_reconstruct = np.copy(img_ms)
    for i in range(img_ms.shape[2]):
        img_reconstruct[:,:,i] = high_pass_fusion(img_ms[:,:,i],img_pan,alpha,N)
    img_reconstruct[img_reconstruct<0] = 0
    img_reconstruct[img_reconstruct>1] = 1 #To avoid problems
    return img_reconstruct
    

## 2nd method: Using img_new = img + alpha*(pan - pan_avg)*img_avg 
def high_pass_fusion_v2(img_to_reconstruct,img_pan,alpha,N):
    #Here the aim is to fusion the panchromatic image with one dimension of the multispectral image
    img_to_reconstruct_mean = local_mean_img(img_to_reconstruct,N)
    img_pan_HF = img_pan - local_mean_img(img_pan,N) #High frequencies of the panchromatic image
    img_reconstruct =img_to_reconstruct+ alpha*img_to_reconstruct_mean * img_pan_HF
    return img_reconstruct

def high_pass_fusion_v2_ms_img(img_ms,img_pan,alpha,N):
    #Here the aim is to reconstruct the multispectral image from the panchromatic one
    img_reconstruct = np.copy(img_ms)
    for i in range(img_ms.shape[2]):
        img_reconstruct[:,:,i] = high_pass_fusion_v2(img_ms[:,:,i],img_pan,alpha,N)
    img_reconstruct[img_reconstruct<0] = 0
    img_reconstruct[img_reconstruct>1] = 1 #To avoid problems
    return img_reconstruct

def high_pass_fusion_adapted_ms_img(img_ms,pan,ms1_response,wavelengths_ms2,alpha=0.7,N=8):
    #In that function I tried to use the best channel of the multispectral image to reconstruct the image
    spectral_response = np.array(ms1_response)
    img_ms_reconstruct = np.zeros(img_ms.shape)
    for i in range(img_ms.shape[2]):
        channel_spectral_response = wavelengths_ms2[i]
        idx_best_channel = np.argmax(spectral_response[spectral_response[:,0] == int(channel_spectral_response),1:])
        value_best_channel = spectral_response[spectral_response[:,0] == int(channel_spectral_response),1:][0,idx_best_channel]
        if value_best_channel < 0.8:
            img_ms_reconstruct[:,:,i] = high_pass_fusion_v2(img_ms[:,:,i],pan,alpha=alpha,N=N)
        else:
            pan_adjusted = high_pass_fusion_v2(img_ms[:,:,idx_best_channel],pan,alpha=alpha,N=N)
            img_ms_reconstruct[:,:,i] = high_pass_fusion_v2(img_ms[:,:,i],pan_adjusted,alpha=alpha,N=N)
    return img_ms_reconstruct

# B. Histogram adjusment:
## 1st method: AML algorithm (img_new = img *(img_ref_avg/img_avg))
def equalize_average(img,img_ref,size_filter):
    #img_lr for low resolution
    local_mean_img_ref = local_mean_img(img_ref,size_filter)
    mean_img = local_mean_img(img,size_filter)
    img_new = img * (local_mean_img_ref/mean_img)
    return img_new

def reconstruct_aml_ms_img(img_ms,img_pan,size_filter):
    img_reconstruct = np.copy(img_ms)
    for i in range(img_ms.shape[2]):
        img_reconstruct[:,:,i] = equalize_average(img_pan,img_ms[:,:,i],size_filter)
    img_reconstruct[img_reconstruct<0] = 0
    img_reconstruct[img_reconstruct>1] = 1 #To avoid problems
    return img_reconstruct

## 2nd method: AMLV algotithm (img_new = (img + (img_ref_avg - img_avg) )/img
def local_ecart_type_img(img,size_filter):
    # compute the local_standard deviation of an image with a filter of size size_filter*size_filter
    mean_img = local_mean_img(img,size_filter)
    img_squared = img**2
    local_mean_img_squared = local_mean_img(img_squared,size_filter)
    var_img = local_mean_img_squared - mean_img**2
    ecart_type_img = np.sqrt(var_img)
    return ecart_type_img

def equalize_var_mean(img,img_ref,size_filter):
    #compute a new image by equalizing the variance and the mean of the image img with the image img_ref
    local_mean_img_ref = local_mean_img(img_ref,size_filter)
    mean_img = local_mean_img(img,size_filter)
    local_ecart_type_img_ref = local_ecart_type_img(img_ref,size_filter)
    ecart_type_img = local_ecart_type_img(img,6)
    img_new = ((img - mean_img) * local_ecart_type_img_ref /ecart_type_img ) + local_mean_img_ref
    return img_new

def reconstruct_amlv_ms_img(img_ms,img_pan,size_filter):
    #recover the multispectral image by equalizing the variance and the mean of each dimension of the image with the panchromatic image
    img_reconstruct = np.copy(img_ms)
    for i in range(img_ms.shape[2]):
        img_reconstruct[:,:,i] = equalize_var_mean(img_pan,img_ms[:,:,i],size_filter)
    img_reconstruct[img_reconstruct<0] = 0
    img_reconstruct[img_reconstruct>1] = 1 #To avoid problems
    return img_reconstruct

# C. PCA:
def reconstruct_pca_img_ms(img_ms,img_pan,nb_components):
    img_reconstruct = np.zeros(img_ms.shape)
    if nb_components>img_ms.shape[2]:
        print('Error: nb_components should be lower than the number of bands')
        print(f"the new number of components is set to {img_ms.shape[2]}")
        nb_components = img_ms.shape[2]
        
    pca = decomposition.PCA(n_components=nb_components, whiten=True)
    img_ms_flatten = img_ms.reshape((img_ms.shape[0]*img_ms.shape[1],img_ms.shape[2]))
    img_ms_flatten_reduced=pca.fit_transform(img_ms_flatten)

    first_component_img = np.reshape(img_ms_flatten_reduced[:,0],(img_ms.shape[0],img_ms.shape[1]))
    pan_adjusted = equalize_var_mean(img_pan,first_component_img,size_filter=6)
    pan_adjusted_flatten = pan_adjusted.flatten()

    img_reconstruct_flatten_reduced = np.copy(img_ms_flatten_reduced)
    img_reconstruct_flatten_reduced[:,0] = pan_adjusted_flatten
    img_reconstruct_flatten = pca.inverse_transform(img_reconstruct_flatten_reduced)

    img_reconstruct = np.reshape(img_reconstruct_flatten,(img_ms.shape[0],img_ms.shape[1],img_ms.shape[2]))
    return img_reconstruct

