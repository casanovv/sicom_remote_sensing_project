"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import cv2
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error
from skimage import img_as_float
from skimage.color import rgb2hsv, hsv2rgb

# Normalisation


def check_img_dimensions(x, y):
    """Check if two images have the same dimensions"""
    assert x.shape == y.shape, "Images have not the same dimension"


def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    check_img_dimensions(x, y)  # Check if the images have the same dimensions

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:, :, i] = norm_std_img(x[:, :, i], y[:, :, i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0, 1))
        y_std = y.std(axis=(0, 1))
        x_norm = y_std * (x - x.mean(axis=(0, 1))) / \
            x.std(axis=(0, 1)) + y_mean
        return x_norm


def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray, type: str) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    M = ms2

    if type == 'General':
        return Sub_gen(pan, M)

    elif type == 'multiplicative':
        return Sub_mult(pan, M)

    elif type == 'LP_gen_box':
        return LP_gen_box(pan, M)

    elif type == 'LP_gen_gauss':
        return LP_gen_gauss(pan, M)

    elif type == 'LP_gen_Laplacian':
        return LP_gen_Laplacian(pan, M)

    elif type == 'LP_mult_box':
        return LP_mult_box(pan, M)

    elif type == 'LP_mult_gauss':
        return LP_mult_gauss(pan, M)

    else:
        print('Error: the type of reconstruction is not correct')
        return None

# Component Subsitution

# General Substituion


def Sub_gen(P, M):
    N = M.shape[-1]
    w = 1/N
    g = 1
    S = np.zeros(M.shape)
    I = np.zeros(P.shape)
    for i in range(N-1):
        I += M[:, :, i] * w
    P = norm_std_img(P, I)
    for i in range(N):
        S[:, :, i] = M[:, :, i] + g*(P-I)
    return (S)

# Multiplicative injection


def Sub_mult(P, M):
    N = M.shape[-1]
    w = 1/N
    S = np.zeros(M.shape)
    I = np.zeros(P.shape)
    for i in range(N):
        I += M[:, :, i] * w
    P = norm_std_img(P, I)
    for i in range(N):
        S[:, :, i] = np.multiply(M[:, :, i], np.divide(P, I))
    return S


# Low pass filtering

# General Subsitution
def LP_gen_box(P, M):
    N = M.shape[-1]
    g = 1
    h_box = np.ones((3, 3), np.float32)/9
    S = np.zeros(M.shape)
    for i in range(N):
        S[:, :, i] = M[:, :, i] + g*(P-cv2.filter2D(P, -1, h_box))
    return S


def LP_gen_gauss(P, M):
    N = M.shape[-1]
    g = 1
    h_gauss = (3, 3)
    sigma = 1
    S = np.zeros(M.shape)
    for i in range(N):
        S[:, :, i] = M[:, :, i] + g*(P-cv2.GaussianBlur(P, h_gauss, sigma))
    return (S)


def LP_gen_Laplacian(P, M):
    N = M.shape[-1]
    g = 1
    S = np.zeros(M.shape)
    for i in range(N):
        S[:, :, i] = M[:, :, i] + g*(P-cv2.Laplacian(P, -1))
    return S

# Multiplicative injection


def LP_mult_box(P, M):
    N = M.shape[-1]
    h_box = np.ones((3, 3), np.float32)/9
    S = np.zeros(M.shape)
    for i in range(N):
        S[:, :, i] = np.multiply(M[:, :, i], np.divide(
            P, cv2.filter2D(P, -1, h_box)))
    return S


def LP_mult_gauss(P, M):
    N = M.shape[-1]
    h_gauss = (3, 3)
    sigma = 1
    S = np.zeros(M.shape)
    for i in range(N):
        S[:, :, i] = np.multiply(M[:, :, i], np.divide(
            P, cv2.GaussianBlur(P, h_gauss, sigma)))
    return S
