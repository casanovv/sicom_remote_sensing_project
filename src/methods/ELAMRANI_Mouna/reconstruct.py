import numpy as np
import cv2
from src.methods.ELAMRANI_Mouna.functions import adjust_image_contrast
from src.methods.ELAMRANI_Mouna.functions import process_images



def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray)-> np.ndarray:
    
    denoised_pan, denoised_ms_mid, denoised_ms_low = process_images(pan, ms1, ms2)

    # Upscaling low-resolution multispectral image
    ms_low_upscaled = cv2.resize(denoised_ms_low, (denoised_ms_mid.shape[1], denoised_ms_mid.shape[0]), interpolation=cv2.INTER_CUBIC)
    
    channels = ms_low_upscaled.shape[2]
    weight_factor = 1 / channels
    integrated_mid = np.zeros(denoised_ms_mid.shape[:2])
    integrated_pan = np.zeros(pan.shape)

    # Calculating integrated values for mid and pan images
    for i in range(channels):
        integrated_mid += ms_low_upscaled[:, :, i] * weight_factor

    scaled_ms_low = np.zeros(ms_low_upscaled.shape)
    
    for channel in range(channels):
        for idx in range(denoised_ms_mid.shape[2]):
            scaled_ms_low[:, :, channel] += ms_low_upscaled[:, :, channel] + \
                np.divide(ms_low_upscaled[:, :, channel], integrated_mid) * (denoised_ms_mid[:, :, idx] - integrated_mid)
    
    scaled_ms_low /= denoised_ms_mid.shape[2]
    scaled_ms_low_upscaled = cv2.resize(scaled_ms_low, (pan.shape[1], pan.shape[0]), interpolation=cv2.INTER_CUBIC)

    for i in range(channels):
        integrated_pan += scaled_ms_low_upscaled[:, :, i] * weight_factor

    normalized_pan = adjust_image_contrast(pan, integrated_pan)

    final_spectral = np.zeros(scaled_ms_low_upscaled.shape)
    # Upscale integrated_pan to match scaled_ms_low_upscaled's dimensions
    integrated_pan_upscaled = cv2.resize(integrated_pan, (scaled_ms_low_upscaled.shape[1], scaled_ms_low_upscaled.shape[0]))

    normalized_pan_upscaled = adjust_image_contrast(pan, integrated_pan_upscaled)

    for channel in range(channels):
        final_spectral[:, :, channel] = scaled_ms_low_upscaled[:, :, channel] + \
        np.divide(scaled_ms_low_upscaled[:, :, channel], integrated_pan_upscaled) * (normalized_pan_upscaled - integrated_pan_upscaled)
    #weighting factors
    res1=0.35 * scaled_ms_low_upscaled
    res2=0.65 * final_spectral


    return res1+res2

