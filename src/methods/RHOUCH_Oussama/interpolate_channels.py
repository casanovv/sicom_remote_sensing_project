import numpy as np

def interpolate_channels(image: np.ndarray, target_channels: int) -> np.ndarray:
    """Interpolate channels of an image to match a target channel count.
    
    Args:
        image: The image to interpolate.
        target_channels: The target channel count.
        
    Returns:
        np.ndarray: The interpolated image.
    """
    
    bands = [[0, 13], [13, 30], [30, 42], [42, 47], [47, 55], [55, 60], [60, 70], [70, 84], [84, 99]]
    
    interpolated = np.zeros((image.shape[0], image.shape[1], target_channels))
    
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for channel, band in enumerate(bands):
                pixel_value = image[i, j, channel] # Get the pixel value of the current channel
                band_channels = np.linspace(band[0], band[1]-1, band[1]-band[0]) # Get the channels of the current band
                interpolated[i, j, band[0]:band[1]] = np.interp(band_channels, [band[0], band[1]-1], [pixel_value, pixel_value]) # Interpolate the pixel value to the current band
                
    return interpolated
