import numpy as np


def pansharpen(ms: np.ndarray, pan: np.ndarray) -> np.ndarray:
    """Pansharpening using the Multiplicative Injection Scheme with additional safeguards.
    
    Args:
        ms: The multispectral image to be pansharpened.
        pan: The panchromatic image.
        
    Returns:
        np.ndarray: The pansharpened multispectral image.
    """
    ms_gray = np.mean(ms, axis=2) # Compute the mean of the three channels.
    ms_gray[ms_gray == 0] = 1 # Avoid division by zero.
    ratio = pan / ms_gray # Compute the ratio between the pan and the mean of the three channels.
    ratio = np.clip(ratio, 0, 2) # Clip the ratio to avoid over-enhancement.
    sharpened = ms * ratio[:, :, np.newaxis] # Apply the ratio to the three channels.
    return sharpened
