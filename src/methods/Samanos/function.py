
from cProfile import label
import numpy as np
from skimage.transform import rescale
import src.utils as utils
from src.methods.Samanos.computation_function import *

def upscale_ms2(img: np.ndarray, shape: tuple) -> np.ndarray:
    """Upscales the ms2 acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): ms2 image.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    return rescale(img, 4, anti_aliasing=True, channel_axis=2)[:shape[0], :shape[1]]

def concatenate_ms1(img: np.ndarray, shape: tuple) -> np.ndarray:
    """Upscales the ms1 acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): ms1 image.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    img1 = np.concatenate((img,img,img),axis=2)
    img1 = np.concatenate((img1,img1,img1,img,img),axis=2)
    return rescale(img1, 2, anti_aliasing=True, channel_axis=2)[:shape[0], :shape[1]]

def rescale_pan(img: np.ndarray, ponderation = np.ones((99))) -> np.ndarray:
    """Upscales the pan acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): pan image.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    img1 = np.zeros((img.shape[0],img.shape[1],99))
    for i in range(99):
        img1[:,:,i] = img * ponderation[i]
    return img1

def evolve_pan(img: np.ndarray) -> np.ndarray:

    filter = get_pan_ponder()

    img1 = np.zeros((img.shape[0],img.shape[1],99))
    for i in range(99):
        img1[:,:,i] = img * filter[i]
    
    return img1

def evolve_ms1(img: np.ndarray, shape: tuple, remove = False, remove_channel = 7, ponder = False) -> np.ndarray:
    """Ponder the ms1 acquisition to the desired number of channels and the desired reconstruction resolution.

    Args:
        img (np.ndarray): ms1 image.
        shape (tuple): Shape of the output.
        remove (bool): Choose to remove a channel
        remove_channel (int): Value of the  channel to remove. Only relevant if remove = True.
        Ponder (bool): Choose how to estimate the channel's image value. 
            Choose True for a pondered sum and False to choose only the most active sensor at this wavelength.

    Returns:
        np.ndarray: Upscaled image.
    """
    # Get the table of the values of the ms1 sensor
    ms1_responses_path = 'data/WV3_VNIRSWIR_Spectral_Responses.csv'
    sensor = utils.get_pan_response(ms1_responses_path).to_numpy()
    # Get the wavelength of the different channels of the image
    wavelengths_path = 'data/Image_wavelengths.csv'
    wl = utils.load_wavelengths(wavelengths_path)
    # Compute the ponderation coefficient of each of the channels along the channels of the ms1 sensor
    if remove :
        # If we want to remove some channels, choose the best channel to remove
        channel = remove_channel
        filter = get_ms1_wavelength_but1(sensor,wl,channel)
    else :
        filter = get_ms1_wavelength(sensor,wl)
    # Define a threshold for the normalisation in order not to divide by 0. The optimum value is 10**-10.
    eps = 10**-10
    img1 = np.zeros((img.shape[0],img.shape[1],99))
    # Compute the 99 version of the ms1 image with the corresponding ponderation coefficients
    alpha = np.float64(0)
    for i in range(99):
        alpha = np.sum(filter[i,:]) # Normalisation factor to have a value in the correct range of values.
        if ponder:
            if alpha > eps : # Threshold under which the normalisation has a meaning
                img1[:,:,i] = multiply_ms1(img,filter,i) / alpha
            else :
                img1[:,:,i] = multiply_ms1(img,filter,i)
        else:
            img1[:,:,i] = closest_ms1(img,filter,i)
    # Upscale the spatial resolution of the image
    res = np.zeros((shape[0],shape[1],99))
    res = upscale_ms1(img1, shape)
    return res

def mean_three_upscales(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray, ponder=(1/3,1/3,1/3)) -> np.ndarray:
    ms1review = evolve_ms1(ms1,pan.shape)
    ms2review = upscale_ms2(ms2,pan.shape)
    panreview = rescale_pan(pan)
    s=sum(ponder)
    ponder = (ponder[0]/s,ponder[1]/s,ponder[2]/s)
    return choose_upscaling_channel(panreview,ms1review,ms2review,ponder)

def improve_ms1(ms1:np.ndarray,pan:np.ndarray) -> np.ndarray:
    res9 = upscale_ms1(ms1,pan.shape)
    rgb_bands = [3,2,0]
    res9[:,:,rgb_bands] = improve_img2hsv(res9[:,:,rgb_bands],pan)
    res99 = augment_dim_ms1(res9)
    return res99

def improve_ms2(ms2:np.ndarray,pan:np.ndarray) -> np.ndarray:
    res = upscale_ms2(ms2,pan.shape)
    rgb_bands = [48, 33, 16]
    res[:,:,rgb_bands] = improve_img2hsv(res[:,:,rgb_bands],pan)
    return res

def pan_ponder_spectrum(img:np.ndarray, pan:np.ndarray, ms1 = False) -> np.ndarray:
    if ms1:
        ponderation=freq_factor_ms1(img)
    else:
        ponderation = freq_factor(img)
    res = rescale_pan (pan,ponderation=ponderation)
    return res

def evolve_linear_combination(pan:np.ndarray, ms1:np.ndarray, ms2 : np.ndarray) -> np.ndarray:
    weightpan = get_pan_ponder()
    weightms1 = get_ms1_ponder()
    weightms2 = get_ms2_ponder()
    for i in range(ms2.shape[2]):
        alpha = weightpan[i] + weightms1[i] + weightms2[i]
        weightpan[i] = weightpan[i]/alpha
        weightms1[i] = weightms1[i]/alpha
        weightms2[i] = weightms2[i]/alpha

    plt.figure()
    plt.plot(weightpan,label="pan")
    plt.plot(weightms1,label="ms1")
    plt.plot(weightms2,label="ms2")
    plt.xlabel("Channel")
    plt.ylabel("Weights")
    plt.legend()
    ms1copy = evolve_ms1(ms1,pan.shape)
    ms2copy = upscale_ms2(ms2,pan.shape)
    res = np.zeros_like(ms2copy)
    for i in range(ms2copy.shape[2]):
        res[:,:,i] = ( ms1copy[:,:,i] * weightms1[i] + ms2copy[:,:,i] * weightms2[i] + pan * weightpan[i] )
    return res