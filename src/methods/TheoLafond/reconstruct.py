"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from scipy.interpolate import griddata

from src.utils import load_wavelengths
from src.utils import get_pan_response, get_ms1_response
from scipy.interpolate import interp1d
from tqdm import tqdm

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """
    Performs data fusion of the three acquisitions using Barnes interpolation.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid-resolution multispectral image.
        ms2 (np.ndarray): Low-resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral image.
    """
    # Define the weight for T and K
    PAN_weight = 0.01
    MS1_weight = 1 - PAN_weight

    # Loads the wavelengths sampling of the image
    wv_path = 'data/Image_wavelengths.csv'
    wv = load_wavelengths(wv_path)

    # Load panchromatic response
    df = get_pan_response('data/QB_Spectral_Response.csv')
    interp = interp1d(df['wavelengths'], df['panchromatic'])
    PAN_filter = interp(wv)

    # Calculate total filter
    total_filtre = PAN_filter * PAN_weight + 0.00001

    # Load multispectral response
    df = get_ms1_response('data/WV3_VNIRSWIR_Spectral_Responses.csv')
    MS1_filter = []
    for k in range(1, df.shape[1]):
        interp = interp1d(df['wavelengths'], df[str(k)])
        filter = interp(wv)
        total_filtre += filter * MS1_weight
        MS1_filter.append(filter)

    # Initialize the output image
    img = np.zeros((pan.shape[0], pan.shape[1], ms2.shape[-1]))

    # Perform data fusion
    for i in tqdm(range(img.shape[0])):
        for j in range(img.shape[1]):
            contrib = np.zeros((ms2.shape[-1]))
            for k in range(len(MS1_filter)):
                # compute the contribution of each band of ms1 already weighted
                contrib += (ms2[i // 4, j // 4, :] * ms1[i // 2, j // 2, k] / np.dot(ms2[i // 4, j // 4, :], MS1_filter[k])) * MS1_filter[k] * MS1_weight
            # compute the contribution of the panchromatic band and normalaze the weightings
            img[i, j, :] = (ms2[i // 4, j // 4, :] * pan[i, j] / np.dot(ms2[i // 4, j // 4, :], PAN_filter) * PAN_filter * MS1_weight + contrib) / (total_filtre)

    return img

    
    
    
if __name__ == "__main__":
    pass



####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####    ########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller and Théo Lafond
