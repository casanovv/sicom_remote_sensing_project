import numpy as np
import cv2

def interpolation(ms_img: np.ndarray, shape: tuple):
    interpolated_img = np.zeros((shape[0], shape[1], ms_img.shape[2]), dtype=ms_img.dtype)
    # Interpolate each band
    for i in range(ms_img.shape[2]):
        # Resize each band using bicubic interpolation
        interpolated_img[:, :, i] = cv2.resize(ms_img[:, :, i], (shape[1], shape[0]), interpolation=cv2.INTER_CUBIC)

    return interpolated_img
