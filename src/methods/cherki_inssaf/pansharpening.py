import numpy as np

def pansharpening( ms_image,pan_image):
    """
    Apply multiplicative injection pansharpening to enhance spatial detail of a multispectral image.
    
    """
    pan_image = pan_image.astype(np.float32)
    ms_image = ms_image.astype(np.float32)
    
    #averaging the multispectral bands to get a synthetic pan band
    synthetic_pan = np.mean(ms_image, axis=2)
    synthetic_pan += np.finfo(np.float32).eps
    
    #the injection ratio
    ratio = pan_image / synthetic_pan
    
    #broadcasting the ratio to all bands of the multispectral image
    ratio = np.expand_dims(ratio, axis=2)
    
    pansharpened_ms = ms_image * ratio
    pansharpened_ms = np.clip(pansharpened_ms, 0, 255).astype(ms_image.dtype)
    
    return pansharpened_ms

