import numpy as np

def check_img_dimensions(x, y):
    """Check if two images have the same dimensions"""
    assert x.shape==y.shape, "Images have not the same dimension"

def norm_minmax(x, lower, upper):
    """Normalize an array to a given bound interval"""

    x_max = np.max(x)
    x_min = np.min(x)

    m = (upper - lower) / (x_max - x_min)
    x_norm = (m * (x - x_min)) + lower

    return x_norm

def norm_std(x, m, s):
    """Normalize an array in order to have a given mean and std"""
    
    x_norm = s * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + m

    return x_norm

def norm_minmax_img(x, y):
    """Normalize an array to a given bound interval given by image y"""
    check_img_dimensions(x, y)

    y_max = np.max(y)
    y_min = np.min(y)

    x_norm = norm_minmax(x, y_min, y_max)
    return x_norm

def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    check_img_dimensions(x, y)

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
    
        # print(y_mean, y_std)

        x_norm = norm_std(x, y_mean, y_std)
        return x_norm