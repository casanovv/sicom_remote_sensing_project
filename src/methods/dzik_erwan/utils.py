"""A file containing some useful functions for the project.
This file should NOT be modified.
"""


from skimage.metrics import peak_signal_noise_ratio, structural_similarity
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from checks import check_data_range, check_shape, check_path, check_csv, check_npy


def get_pan_response(file_path: str) -> np.ndarray:
    """Loads the panchromatic spectral response.

    Args:
        file_path (str): Path to the file containing the response. Must end by '.csv'.

    Returns:
        np.ndarray: Panchromatic response.
    """
    check_path(file_path)
    check_csv(file_path)

    return pd.read_csv(file_path)


def get_ms1_response(file_path: str) -> np.ndarray:
    """Loads the ms1 spectral responses.

    Args:
        file_path (str): Path to the file containing the responses. Must end by '.csv'.

    Returns:
        np.ndarray: ms1 response.
    """
    check_path(file_path)
    check_csv(file_path)

    return pd.read_csv(file_path)


def load_wavelengths(file_path: str) -> np.ndarray:
    """Loads the wavelengths in an array.

    Args:
        file_path (str): Path to the file containing the wavelengths. Must end by '.csv'.

    Returns:
        np.ndarray: Wavelengths.
    """
    check_path(file_path)
    check_csv(file_path)

    df = pd.read_csv(file_path)

    return df['wavelengths'].values


def load_image(file_path: str) -> np.ndarray:
    """Loads the image located in file_path.

    Args:
        file_path (str): Path to the file containing the Moffett image. Must end with '.npy'.

    Returns:
        np.ndarray: Loaded image.
    """
    check_path(file_path)
    check_npy(file_path)

    res = np.load(file_path)

    return (res - np.min(res)) / np.ptp(res)


def save_image(file_path: str, img: np.ndarray) -> None:
    """Saves the image located in file_path.

    Args:
        file_path (str): Path to the file in which the image will be saved. Must end by '.npy'.
        img (np.ndarray): Image to save.
    """
    check_path(file_path.split('/')[-2])
    check_npy(file_path)

    np.save(file_path, img)


def show_img(img: np.ndarray, title: str, cmap: str ='gray') -> None:
    """Shows a grayscale or RGB image after performing a stretching of its dynamic.

    Args:
        img (np.ndarray): Image to show.
        title (str): Title of to print.
        cmap (str, optional): Color map used to show the image. Defaults to 'gray'.
    """
    nbins = 1000
    tol_low = 0.1
    tol_high = 0.9
    res = np.zeros(img.shape)
    n_bands = 1 if len(img.shape) < 3 else img.shape[2]

    for i in range(n_bands):
        tmp = img if n_bands == 1 else img[:, :, i]
        tmp = tmp - np.min(tmp.ravel())
        tmp = tmp / np.max(tmp.ravel())
        N, _ = np.histogram(tmp.ravel(), nbins)
        cdf = np.cumsum(N)/np.sum(N)  # cumulative distribution function
        ilow = np.where(cdf > tol_low)[0][0]
        ihigh = np.where(cdf >= tol_high)[0][0]
        ilow = (ilow - 1)/(nbins-1)
        ihigh = (ihigh - 1)/(nbins-1)
        li = ilow
        lo = 0
        hi = ihigh
        ho = 255
        out = (tmp < li) * lo
        out = out + np.logical_and(tmp >= li, tmp < hi) * (lo + (ho - lo) * ((tmp - li) / (hi - li)))
        out = out + (tmp >= hi) * ho

        if n_bands == 1:
            res = out
        else:
            res[:, :, i] = out

    plt.figure(figsize=(20, 20))
    plt.title(title)
    plt.imshow(res.astype('uint8'), cmap=cmap)


def psnr(img1: np.ndarray, img2: np.ndarray) -> float:
    """Computes the PSNR between img1 and img2 after some sanity checks.
    img1 and img2 must:
        - have the same shape;
        - be in range [0, 1].

    Args:
        img1 (np.ndarray): First image.
        img2 (np.ndarray): Second image.

    Returns:
        float: PSNR between img1 and img2.
    """
    check_shape(img1, img2)
    check_data_range(img1)

    return peak_signal_noise_ratio(img1, img2, data_range=1)


def ssim(img1: np.ndarray, img2: np.ndarray) -> float:
    """Computes the SSIM between img1 and img2 after some sanity checks.
    img1 and img2 must:
        - have the same shape;
        - be in range [0, 1];
        - be 3 dimensional array with 3 channels.

    Args:
        img1 (np.ndarray): First image.
        img2 (np.ndarray): Second image.

    Returns:
        float: SSIM between img1 and img2.
    """
    check_shape(img1, img2)
    check_data_range(img1)
    check_data_range(img2)

    return structural_similarity(img1, img2, data_range=1, channel_axis=2)


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
