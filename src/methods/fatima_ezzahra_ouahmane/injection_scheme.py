import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize

def multiplicative_injection_scheme(M, P):
    N = M.shape[2]
    gain_factors = np.zeros_like(M)

    for k in range(N):
        gain_factors[:, :, k] = M[:, :, k] / (np.sum(M, axis=2) + 1e-8)

    multiplicative_pansharpened_image = np.zeros_like(M)

    for k in range(N):
        multiplicative_pansharpened_band = M[:, :, k] * gain_factors[:, :, k]
        multiplicative_pansharpened_band = np.clip(multiplicative_pansharpened_band, 0, 1)
        multiplicative_pansharpened_image[:, :, k] = multiplicative_pansharpened_band

    return multiplicative_pansharpened_image