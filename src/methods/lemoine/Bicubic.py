import numpy as np
import cv2

## Upsampling with the bicubic interpolation method
def bicubic_interpolation(pan, ms):

    L, l = pan.shape
    ms_interpolated = cv2.resize(ms, (l, L), pan, interpolation=cv2.INTER_CUBIC)

    return ms_interpolated