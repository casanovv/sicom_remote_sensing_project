import numpy as np
import pywt

## Wavelet method for pansharpening

def Wavelet(pan, ms):

    M, N = pan.shape
    m, n, C = ms.shape
    
    pan = np.squeeze(pan)
    pan_wav = pywt.wavedec2(pan, 'haar', level = 2)             # Apply wavelet transform on pan

    res = []
    for i in range(C):
        ms_wav = pywt.wavedec2(ms[:, :, i], 'haar', level = 2)  # Apply wavelet transform on ms

        pan_wav[0] = ms_wav[0]
  
        inv = pywt.waverec2(pan_wav, 'haar')                    # Inverse transform
        inv = np.expand_dims(inv, -1)
        res.append(inv)

    
    I_Wavelet = np.concatenate(res, axis=-1)

    return I_Wavelet