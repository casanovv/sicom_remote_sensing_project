# Normalisation
def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    assert x.shape==y.shape, "Images have not the same dimension"

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
        x_norm = y_std * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + y_mean
        return x_norm