# -*- coding: utf-8 -*-
"""
License: MIT
@author: gaj
E-mail: anjing_guo@hnu.edu.cn 
"""

import numpy as np
from src.methods.lioretn.utils import upsample_bicubic

def Bicubic(pan, ms):
    M, N, c = pan.shape
    m, n, C = ms.shape

    # Check that the format of pan and ms image are similar
    assert int(np.round(M/m)) == int(np.round(N/n))
    
    # Upsample with bicubic
    I_Bicubic = upsample_bicubic(ms, (N, M))
    
    # Adjustment
    I_Bicubic[I_Bicubic<0]=0
    I_Bicubic[I_Bicubic>1]=1

    return np.float64(I_Bicubic)