"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import cv2
from src.utils import load_wavelengths, load_image, save_image, show_img, psnr, ssim



# Normalisation
def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    assert x.shape==y.shape, "Images have not the same dimension"

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
        x_norm = y_std * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + y_mean
        return x_norm
    

 # Several pansharpening method implementation
    
## Component Substitution

### Additive injection




def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray ) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    ### We will use the choose of interpolation -> cubic

    ms2 = cv2.resize(ms2, (pan.shape[1], pan.shape[0]), pan, interpolation=cv2.INTER_CUBIC)
    ms1 = cv2.resize(ms1, (pan.shape[1], pan.shape[0]), pan, interpolation=cv2.INTER_CUBIC)
    
    M = ms2 

    ### During my work, I added a 'mode' argument to allow me to do this or that treatment, 
    ### so I could choose the method used. To do the job again without touching the argument, I force 'mode' = 'multiplicative'.

    mode = 'multiplicative'

    if mode == 'Substitution' :
        M = ms1
        N =  M.shape[-1]  
        S = np.zeros( M.shape)
        for i in range(N):
            if i == 7 : 
                S[:,:,i] = pan
            else : 
                S[:,:,i] =  M[:,:,i]
        return S 


    elif mode == 'additive':
        N =  M.shape[-1]                                  
        w = 1/N                                         # Same value of the weights for each band
        g = 1                                           # No gain correction -- Same for each methods
        S = np.zeros( M.shape)
        I = np.zeros(pan.shape)
        for i in range (N-1):
            I +=  M[:,:,i] * w 
        pan = norm_std_img(pan, I)                          # Normalisation of the Pan image to have the same range value of I
        for i in range (N):
            S[:,:,i] =  M[:,:,i] + g*(pan-I)
        return S
    
    elif mode == 'additive_lowpass' :

        N =  M.shape[-1]                                
        g = 1
        hlp = np.ones((3,3),np.float32)/9 #Definition of filter
        S = np.zeros( M.shape)
        for i in range (N):
            S[:,:,i] =  M[:,:,i] + g*(pan-cv2.filter2D(pan,-1,hlp))
        return S
    
    elif mode == 'additive_Laplacian' :

        N =  M.shape[-1]                                   
        g = 1
        S = np.zeros( M.shape)
        for i in range (N):
            S[:,:,i] =  M[:,:,i] + g*(pan-cv2.Laplacian(pan,-1))
        
        return S
    
    elif mode == 'additive_gauss' :

        N =  M.shape[-1]                                   
        g = 1
        hg_size = (3,3) #Definition of filter
        sigma = 1
        S = np.zeros( M.shape)
        for i in range (N):
            S[:,:,i] =  M[:,:,i] + g*(pan-cv2.GaussianBlur(pan,hg_size,sigma))
 
        return S
    
    elif mode == 'multiplicative' :
        N =  M.shape[-1]                                  
        w = 1/N                                           
        S = np.zeros( M.shape)
        I = np.zeros(pan.shape)
        for i in range (N-1):
            I +=  M[:,:,i] * w 
        pan = norm_std_img(pan, I)                       
        for i in range (N):
            S[:,:,i] =  np.multiply(M[:,:,i],np.divide(pan,I))
        return S
    
    elif mode == 'multiplicative_lowpass' :
        N = M.shape[-1]                                 
        hlp = np.ones((3,3),np.float32)/9 #Definition of filter
        S = np.zeros(M.shape)
        for i in range (N):
            S[:,:,i] = M[:,:,i] + (pan-cv2.filter2D(pan,-1,hlp))
        return S
    
    elif mode == 'multiplicative_gauss' :
        N = M.shape[-1]                                  
        hg_size = (11,11) #Definition of filter 
        sigma = 1
        S = np.zeros(M.shape)
        for i in range (N):
            S[:,:,i] = np.multiply(M[:,:,i],np.divide(pan,cv2.GaussianBlur(pan,hg_size, sigma)))
        return S
    elif mode =='evaluate_add_gauss' :
        ## THE RETURN CHANGE 
        image_path = 'data/image.npy'
        img = load_image(image_path)
        sigma = np.array([0.01,0.1,1,5,10,100]) #Definition of filter
        N =  M.shape[-1]                                  
        hg_size = (3,3)
        PSNR = np.zeros(sigma.shape[0])
        SSIM = np.zeros(sigma.shape[0])
        for n in range(sigma.shape[0]):
            S = np.zeros( M.shape)
            for i in range (N):
                S[:,:,i] =  M[:,:,i] + (pan-cv2.GaussianBlur(pan,hg_size,sigma[n]))
            PSNR[n] =psnr(img, S) 
            SSIM[n] = ssim(img, S)
        return (PSNR,SSIM)

    else :
        pass

    





























































####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
