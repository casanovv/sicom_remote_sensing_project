"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from src.methods.vouillon.kernel import gaussian_kernel
import scipy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from scipy import signal
from src.utils import load_wavelengths


def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    

    
    wvl_path = 'data/Image_wavelengths.csv'

    wvl = load_wavelengths(wvl_path)

    #firs linear interpolation
    ms2_i = np.zeros((154,640,99))
    for i in range (99):    
        ms2_i[:,:,i] = scipy.ndimage.zoom(ms2[:,:,i], 2, order=1)


    # Creating spectral band batches
    wv1 = [400, 450]
    wv2 = [450,525]
    wv3 = [525,570]
    wv4 = [570,630]
    wv5 = [630,700]
    wv6 = [700,750]
    wv7 = [750,875]
    wv8 = [875,1100]
    wv9 = [1100,1350]

    intervals = [wv1, wv2, wv3, wv4, wv5, wv6, wv7, wv8, wv9]

    batches = [np.array([]) for i in range (9)]

    for wl in wvl:
        for i,interval in enumerate(intervals):
            if wl >= interval[0] and wl < interval[1]:
                batches[i] = np.append(batches[i],wl)
                continue

    
    # Performing HPF pansharpening on every batch
    

    for b,batch in enumerate(batches):


        S = np.zeros(np.shape(ms2_i))

        N = 99
        w = 1/N*np.ones(N)
        g = np.ones(N)

        H_LP = gaussian_kernel(5, 1)



        pan_conv = signal.convolve2d(ms1[:,:,b], H_LP, mode='same', boundary='fill')

        for i in range (N): # Additive scheme
            S[:,:,i] = ms2_i[:,:,i]+g[i]*(ms1[:,:,b]-pan_conv)
        


    #second linear interpolation

    ms1_new_i = np.zeros((308,1280,99))
    for i in range (99):    
        ms1_new_i[:,:,i] = scipy.ndimage.zoom(S[:,:,i], 2, order=1)

    ms1_new_i = ms1_new_i[0:-1,:,:]


    # CS pansharpening

    N = 99
    w = 1/N*np.ones(N)
    g = np.ones(N)

    I = np.zeros(np.shape(ms1_new_i[:,:,0]))
    
    for i in range (N):
        I+=w[i]*ms1_new_i[:,:,i]
    
    img_new = np.zeros(np.shape(ms1_new_i))
   
    for i in range (N):
        img_new[:,:,i]=ms1_new_i[:,:,i]+g[i]*(pan-I)

    img_new[img_new > 1] = 1
    img_new[img_new < 0] = 0

    return img_new



####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
